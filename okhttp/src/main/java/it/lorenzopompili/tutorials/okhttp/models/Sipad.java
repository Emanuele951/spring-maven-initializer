package it.lorenzopompili.tutorials.okhttp.models;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Lorenzo Pompili - 28/09/20 - 16:32
 **/
@Entity
@Table(name = "SIPAD")
public class Sipad {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Long id;

  @Column(name = "FORZA_ARMATA_DI_APPARTENENZA")
  private String forzaArmataDiAppartenenza;

  @Column(name = "SPECIALITA_FORZA_ARMATA_DI_APPARTENENZA")
  private String specialitaForzaArmataDiAppartenenza;

  @Column(name = "GRADO_MILITARE")
  private String gradoMilitare;

  @Column(name = "COMANDO_RFC_DI_APPARTENENZA")
  private String comandoRFCDiAppartenenza;

  @Column(name = "ENTE_DI_APPARTENENZA_CON_CITTA")
  private String enteDiAppartenenzaConCitta;

  @Column(name = "CATEGORIA_SPECIALITA_INCARICO")
  private String categoriaSpecialitaIncarico;

  @Column(name = "STATO_CONGEDATO")
  private String statoCongedato;

  @Column(name = "TIPO_DI_FERMA")
  private String tipoDiFerma;

  @Column(name = "TIPO_DI_RAFFERMA")
  private String tipoDiRafferma;

  @Column(name = "DATA_INIZIO_DEL_SERVIZIO_INCORPORAZIONE")
  private Date dataInizioDelServizioIncorporazione;

  @Column(name = "DATA_CONGEDO_SE_PRESENTE")
  private Date dataCongedoSePresente;

}
