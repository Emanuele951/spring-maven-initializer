package it.lorenzopompili.tutorials.okhttp.utilities;

import com.google.gson.Gson;
import java.io.IOException;
import java.security.PublicKey;
import okhttp3.Call;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Value;

/**
 * Lorenzo Pompili - 28/09/20 - 16:50
 **/
public class ConnectionOkHttpUtils {

  private ConnectionOkHttpUtils() {
  }

  public static Response connect(String url, String port, String apiMethod, String codiceFiscale)
      throws IOException {

    OkHttpClient client = new OkHttpClient();

    RequestBody requestBody = RequestBody.create(codiceFiscale,
        MediaType.parse(("application/*")));

    Request request = new Request.Builder()
        .url(getURL(url,port,apiMethod))
        .post(requestBody)
        .build();

    Call call = client.newCall(request);

    return call.execute();

  }

  public static String getURL(String url, String port, String apiMethod) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("http://");
    stringBuilder.append(url);
    stringBuilder.append(":");
    stringBuilder.append(port);
    stringBuilder.append("/");
    stringBuilder.append(apiMethod);

    return stringBuilder.toString();
  }

  public static String getURL(String url, String apiMethod) {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("http://");
    stringBuilder.append(url);
    stringBuilder.append("/");
    stringBuilder.append(apiMethod);

    return stringBuilder.toString();
  }

}
