package it.lorenzopompili.tutorials.okhttp.models;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Lorenzo Pompili - 28/09/20 - 16:08
 **/
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "ANPAL")
@Entity
public class Anpal {


  @Id
  @Column(name = "ID")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "DATA_CESSAZIONE")
  private Date dataCessazione;

  @Column(name = "DATA_ARRUOLAMENTO")
  private Date dataArruolamento;

  @Column(name = "FORZA_ARMATA")
  private String forzaArmata;

  @Column(name = "DATA_DECESSO")
  private Date dataDecesso;

  @Column(name = "TITOLO_STUDIO")
  private String TTITOLO_STUDIO;

  @Column(name = "AIDDP_MAIL_PEI")
  private String aiddpMailPei;

  @Column(name = "AIDDP_MAIL_PEC")
  private String aiddpMailPec;

  @Column(name = "TELEFONO")
  private String telefono;

  @Column(name = "CAP_RESIDENZA")
  private String capResidenza;

  @Column(name = "IND_RESIDENZA")
  private String indResidenza;

  @Column(name = "COMUNE_RESIDENZA")
  private String comuneResidenza;

  @Column(name = "NAZIONE_RESIDENZA")
  private String nazioneResidenza;

  @Column(name = "CAP_IND_DOMICILIO")
  private String capIndDomicilio;

  @Column(name = "IND_DOMICILIO")
  private String indDomicilio;

  @Column(name = "COMUNE_DOMICILIO")
  private String comuneDomicilio;

  @Column(name = "DESCR_STATO_CIVILE")
  private String descrStatoCivile;

  @Column(name = "COMUNE_NASCITA")
  private String comuneNascita;

  @Column(name = "NAZIONE_NASCITA")
  private String nazioneNascita;

  @Column(name = "DATA_NASCITA")
  private Date dataNascita;

  @Column(name = "MATRICOLA")
  private String matricola;

  @Column(name = "SESSO")
  private Character sesso;

  @Column(name = "NOME")
  private String nome;

  @Column(name = "COGNOME")
  private String cognome;

  @Column(name = "CODICE_FISCALE")
  private String codiceFiscale;

  @Column(name = "CIVILE")
  private String civile;

  @Column(name = "STATO_GIURIDICO")
  private String statoGiuridico;

  @Column(name = "POSIZIONE_MILITARE")
  private String posizioneMilitare;

  @Column(name = "INCARICO")
  private String incarico;

  @Column(name = "IN_FORZA")
  private String inForza;

  @Column(name = "CATEGORIA")
  private String categoria;

  @Column(name = "ABILITAZIONE")
  private String abilitazione;

  @Column(name = "SPECIALITA")
  private String specialita;

  @Column(name = "GRADO")
  private String grado;

  @Column(name = "RUOLO")
  private String ruolo;

  @Column(name = "CORPO")
  private String corpo;

  @Column(name = "DESC_ENTE_APPARTENENZA")
  private String descEnteAppartenenza;

  @Column(name = "ENTE_APPARTENENZA")
  private String enteAppartenenza;
}
