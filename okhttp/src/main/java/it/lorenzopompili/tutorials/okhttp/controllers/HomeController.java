package it.lorenzopompili.tutorials.okhttp.controllers;

import it.lorenzopompili.tutorials.okhttp.models.Anpal;
import it.lorenzopompili.tutorials.okhttp.models.Sipad;
import it.lorenzopompili.tutorials.okhttp.repositories.AnpalRepository;
import it.lorenzopompili.tutorials.okhttp.repositories.SipadRepository;
import java.util.Collection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Lorenzo Pompili - 28/09/20 - 16:22
 **/
@RestController
public class HomeController {

  @Autowired
  private AnpalRepository anpalRepository;

  @Autowired
  private SipadRepository sipadRepository;


  @PostMapping("/anpalSelectAll")
  public Collection<Anpal> anpalSelectAll() {
    return anpalRepository.findAll();
  }

  @PostMapping("/anpalSelectByCF")
  public Collection<Anpal> anpalSelectByCF(@RequestBody String codiceFiscale) {
    return anpalRepository.findAllByCodiceFiscale(codiceFiscale);
  }

  @PostMapping("/sipadSelectAll")
  public Collection<Sipad> sipadSelectAll() {
    return sipadRepository.findAll();
  }


}
