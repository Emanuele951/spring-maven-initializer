package it.lorenzopompili.tutorials.okhttp.repositories;

import it.lorenzopompili.tutorials.okhttp.models.Anpal;
import java.util.Collection;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Lorenzo Pompili - 28/09/20 - 16:21
 **/
public interface AnpalRepository extends JpaRepository<Anpal, Long> {

   Collection<Anpal> findAllByCodiceFiscale(String codiceFiscale);

}
