package it.lorenzopompili.tutorials.okhttp.repositories;

import it.lorenzopompili.tutorials.okhttp.models.Sipad;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Lorenzo Pompili - 28/09/20 - 16:33
 **/
public interface SipadRepository extends JpaRepository<Sipad,Long> {

}
