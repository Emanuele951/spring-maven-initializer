package it.lorenzopompili.tutorials.okhttp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TutorialOkhttpApplication {

	public static void main(String[] args) {
		SpringApplication.run(TutorialOkhttpApplication.class, args);
	}

}
