package it.lorenzopompili.tutorials.okhttp;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import it.lorenzopompili.tutorials.okhttp.models.Anpal;
import it.lorenzopompili.tutorials.okhttp.utilities.ConnectionOkHttpUtils;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import okhttp3.Response;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class TutorialOkhttpApplicationTests {

  @Test
  void anpalTest() throws IOException {

    Anpal anpalTest = new Anpal();
    anpalTest.setCodiceFiscale("PMPLNZ89M16H501A");
    anpalTest.setGrado("Capitano");
    anpalTest.setSesso('m');

    Response connect = ConnectionOkHttpUtils
        .connect("localhost", "8081", "okhttptest/anpalSelectByCF", "PMPLNZ89M16H501A");

    Type anpalType = new TypeToken<List<Anpal>>() {
    }.getType();
    Collection<Anpal> anpalList = new Gson()
        .fromJson(Objects.requireNonNull(connect.body(), "ERRORE TERRIBILE").string(), anpalType);


    List<Anpal> anpalListFiltered = anpalList.parallelStream()
        .filter(anpal -> anpal.getCodiceFiscale().toUpperCase().equals(anpalTest.getCodiceFiscale().toUpperCase()))
        .filter(anpal -> anpal.getGrado().toUpperCase().equals(anpalTest.getGrado().toUpperCase()))
        .filter(anpal -> Character.toUpperCase(anpal.getSesso()) == (Character.toUpperCase(anpalTest.getSesso())))
        .collect(Collectors.toList());

    System.out.println(anpalListFiltered);
    System.out.println(anpalList);

  }

}
