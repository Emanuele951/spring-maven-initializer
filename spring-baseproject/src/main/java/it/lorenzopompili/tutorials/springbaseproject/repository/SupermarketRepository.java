package it.lorenzopompili.tutorials.springbaseproject.repository;

import it.lorenzopompili.tutorials.springbaseproject.models.Supermarket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Lorenzo Pompili - 26/09/20 - 13:16
 **/
@Repository
public interface SupermarketRepository extends JpaRepository<Supermarket, Long> {

}
