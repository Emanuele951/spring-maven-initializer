package it.lorenzopompili.tutorials.springbaseproject.services;

import it.lorenzopompili.tutorials.springbaseproject.models.User;
import it.lorenzopompili.tutorials.springbaseproject.repository.UserRepository;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Lorenzo Pompili - 26/09/20 - 15:15
 **/
@Service
public class UserService {
  @Autowired
  private UserRepository userRepository;

  public User save(User user) {
    return userRepository.save(user);
  }

  public Optional<User> getByToken(String token) {
    return userRepository.findByToken(token);
  }

  public Optional<User> getByUsername(String username) {
    return userRepository.findByUsername(username);
  }
}
