package it.lorenzopompili.tutorials.springbaseproject.services;

import it.lorenzopompili.tutorials.springbaseproject.models.User;
import it.lorenzopompili.tutorials.springbaseproject.utilities.PBKDF2;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Optional;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Service;

/**
 * Lorenzo Pompili - 26/09/20 - 15:14
 **/
@Profile({"dev-uuid", "prod-uuid"})
@Service
public class UUIDAuthenticationService implements UserAuthenticationService {

  @Autowired
  private UserService userService;

  @Override
  public String login(String username, String password)
      throws InvalidKeySpecException, NoSuchAlgorithmException {

//    User userFromDB = userService.getByUsername(username).get();

//    if (password.equals(PBKDF2.generatePassword(password))) {
//      userFromDB.setToken(UUID.randomUUID().toString());
//      userService.save(userFromDB);
//      return userFromDB.getToken();
//    } else {
//      throw new BadCredentialsException("Invalid username or password.");
//    }

    return userService.getByUsername(username)
        .filter(u -> u.getPassword().equals(password))
        .map(u -> {
          u.setToken(UUID.randomUUID().toString());
          userService.save(u);
          return u.getToken();
        })
        .orElseThrow(() -> new BadCredentialsException("Invalid username or password."));

  }

  @Override
  public User authenticateByToken(String token) {
    return userService.getByToken(token)
        .orElseThrow(() -> new BadCredentialsException("Token not found."));
  }

  @Override
  public void logout(String username) {
    userService.getByUsername(username)
        .ifPresent(u -> {
          u.setToken(null);
          userService.save(u);
        });
  }
}