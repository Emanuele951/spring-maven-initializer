package it.lorenzopompili.tutorials.springbaseproject.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.web.RedirectStrategy;

/**
 * Lorenzo Pompili - 26/09/20 - 15:04
 **/
public class NoRedirectStrategy implements RedirectStrategy {

  @Override
  public void sendRedirect(HttpServletRequest request, HttpServletResponse response, String url)  {
  }
}