package it.lorenzopompili.tutorials.springbaseproject.services;

import it.lorenzopompili.tutorials.springbaseproject.models.User;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Lorenzo Pompili - 26/09/20 - 15:19
 **/
@Service
public class UserRegistrationService {

  @Autowired
  private UserService userService;

  @Autowired
  private UserAuthenticationService userAuthenticationService;

  public String register(String username, String password)
      throws InvalidKeySpecException, NoSuchAlgorithmException {

    userService.getByUsername(username)
        .ifPresent(u -> {
          throw new IllegalArgumentException("Username already in use.");
        });

    User user = new User();
    user.setUsername(username);
    user.setPassword(password);
//    user.setPassword(PBKDF2.generatePassword(password));
    user.setPassword(password);
    userService.save(user);

    return userAuthenticationService.login(username, password);
  }
}