package it.lorenzopompili.tutorials.springbaseproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBaseprojectApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBaseprojectApplication.class, args);
	}

}
