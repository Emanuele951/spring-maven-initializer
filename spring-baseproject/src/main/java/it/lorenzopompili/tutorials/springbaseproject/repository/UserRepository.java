package it.lorenzopompili.tutorials.springbaseproject.repository;

import it.lorenzopompili.tutorials.springbaseproject.models.User;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Lorenzo Pompili - 26/09/20 - 14:29
 **/
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

  Optional<User> findByUsername(String username);

  Optional<User> findByToken(String token);
}
