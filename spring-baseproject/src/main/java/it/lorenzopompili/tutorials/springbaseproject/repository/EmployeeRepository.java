package it.lorenzopompili.tutorials.springbaseproject.repository;

import it.lorenzopompili.tutorials.springbaseproject.models.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Lorenzo Pompili - 26/09/20 - 14:29
 **/
@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

}
