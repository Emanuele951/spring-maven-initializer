package it.lorenzopompili.tutorials.springbaseproject.controllers;

import static org.springframework.http.HttpStatus.I_AM_A_TEAPOT;
import static org.springframework.http.HttpStatus.UNAUTHORIZED;

import it.lorenzopompili.tutorials.springbaseproject.services.UserAuthenticationService;
import it.lorenzopompili.tutorials.springbaseproject.services.UserRegistrationService;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Lorenzo Pompili - 26/09/20 - 15:18
 **/

@RestController
public class PublicEndpointsController {

  @Autowired
  private UserRegistrationService registrationService;

  @Autowired
  private UserAuthenticationService userAuthenticationService;

  @PostMapping("/register")
  public Object register(
      @RequestParam("username") String username, @RequestParam("password") String password) {
    try {
      return registrationService
          .register(username, password);
    } catch (Exception e) {
      return ResponseEntity.badRequest().body(e.getMessage());
    }
  }

  @PostMapping("/login")
  public Object login(@RequestParam("username") String username,
      @RequestParam("password") String password) {
    try {
      return userAuthenticationService
          .login(username, password);
    } catch (BadCredentialsException | InvalidKeySpecException | NoSuchAlgorithmException e) {
      e.printStackTrace();
      return ResponseEntity.status(UNAUTHORIZED).body(e.getMessage());
    }
  }

  @PostMapping("/logout")
  public Object logout(@RequestParam("username") String username) {
    try {
      userAuthenticationService
          .logout(username);
      return ResponseEntity.ok();
    } catch (Exception e) {
      return ResponseEntity.status(I_AM_A_TEAPOT).body(e.getMessage());
    }
  }
}