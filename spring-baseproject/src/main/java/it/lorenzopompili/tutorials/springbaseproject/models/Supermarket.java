package it.lorenzopompili.tutorials.springbaseproject.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Lorenzo Pompili - 26/09/20 - 13:08
 **/
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "SUPERMARKETS")
public class Supermarket {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "ID")
  private Long supermarketId;

  @Column(name = "NAME")
  @NotNull
  private String name;

  @Column(name = "ADDRESS")
  @NotNull
  private String address;

  @Column(name = "CITY")
  @NotNull
  private String city;

}
