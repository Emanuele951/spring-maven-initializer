package it.lorenzopompili.tutorials.springbaseproject.controllers;

import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Lorenzo Pompili - 26/09/20 - 13:19
 **/

@Log4j2
@RestController
@RequestMapping("/home")
@CrossOrigin
public class HomeController {

  @GetMapping("/welcome")
  public ResponseEntity<String> welcomeController() {
    log.warn("welcomeController is responding");
    return ResponseEntity.ok("fucking juicy");
  }

}
