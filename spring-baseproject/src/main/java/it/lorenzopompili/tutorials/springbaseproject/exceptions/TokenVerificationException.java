package it.lorenzopompili.tutorials.springbaseproject.exceptions;

/**
 * Lorenzo Pompili - 27/09/20 - 11:57
 **/
public class TokenVerificationException extends RuntimeException {

  public TokenVerificationException(Throwable t) {
    super(t);
  }
}