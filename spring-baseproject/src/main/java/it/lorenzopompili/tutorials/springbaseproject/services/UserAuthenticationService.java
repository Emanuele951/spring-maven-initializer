package it.lorenzopompili.tutorials.springbaseproject.services;

import it.lorenzopompili.tutorials.springbaseproject.models.User;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import org.springframework.stereotype.Service;

/**
 * Lorenzo Pompili - 26/09/20 - 15:02
 **/
@Service
public interface UserAuthenticationService {

  String login(String username, String password)
      throws InvalidKeySpecException, NoSuchAlgorithmException;

  User authenticateByToken(String token);

  void logout(String username);

}
