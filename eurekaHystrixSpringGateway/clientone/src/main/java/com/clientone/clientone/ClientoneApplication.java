package com.clientone.clientone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class ClientoneApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClientoneApplication.class, args);
	}

}
