package com.clientone.clientone.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Lorenzo Pompili - 20/09/20 - 17:07
 **/
@RestController
@RequestMapping("clientone")
public class HomeController {

  @GetMapping("/home")
  public ResponseEntity<String> homeTest() {
    return ResponseEntity.ok("client one is here");
  }

}
