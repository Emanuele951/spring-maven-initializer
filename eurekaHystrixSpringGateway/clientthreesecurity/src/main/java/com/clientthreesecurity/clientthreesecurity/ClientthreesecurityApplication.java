package com.clientthreesecurity.clientthreesecurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class ClientthreesecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClientthreesecurityApplication.class, args);
	}

}
