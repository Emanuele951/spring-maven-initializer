package com.clienttwo.clienttwo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class ClienttwoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClienttwoApplication.class, args);
	}

}
