package com.gatewaytest.gateway.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Lorenzo Pompili - 20/09/20 - 17:33
 **/
@RestController
@RequestMapping("/fallback")
public class FallBackController {

  @GetMapping("/message")
  public String test() {
    return "HYSTRIX IS WORKING, QUALCUNO SISTEMERA' E NON SARA' PASTA";

  }
}
