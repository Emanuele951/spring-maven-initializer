package com.serverone.serverone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class ServeroneApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServeroneApplication.class, args);
	}

}
