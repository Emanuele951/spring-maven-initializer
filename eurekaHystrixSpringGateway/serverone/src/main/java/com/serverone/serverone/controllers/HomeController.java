package com.serverone.serverone.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Lorenzo Pompili - 20/09/20 - 16:42
 **/
@RestController
@RequestMapping("serverone")
public class HomeController {

  @GetMapping("home")
  public ResponseEntity<String> homeMethod() {
    return ResponseEntity.ok("server one is here!");
  }
}
